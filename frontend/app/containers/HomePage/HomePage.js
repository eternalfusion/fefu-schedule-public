import React, { useEffect, useState } from 'react'
// import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import DefaultHeader from '../../components/Header'
import LocaleToggle from '../../components/LocaleToggle'
import { FormattedMessage } from 'react-intl'
import FindPage from "../../components/FindPage";
import TestSwitcher from "./components/TestSwitcher";

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		height: '100%',
		width: '100%'
	},
	//toolbar: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		padding: theme.spacing(10),
	},
}))

const HomePage = ({}) => {

	const classes = useStyles()

	/*const content = []

	const [ transparency, setTransparency ] = useState(true)

	useEffect(() => {
		window.addEventListener('scroll', handleScroll)
		return () => window.removeEventListener('scroll', handleScroll)
	}, [])

	const handleScroll = () => {
		// TODO remove hardcodedd value.
		setTransparency(document.documentElement.scrollTop <= 80)
	}*/

	return (
		<div className={ classes.root }>
				<TestSwitcher/>
		</div>
	)
}

HomePage.propTypes = {}

export default HomePage