import React from 'react'
import PropTypes from 'prop-types'
import {makeStyles} from '@material-ui/core/styles'
import {useGroup} from "../../../components/ReactSelect/reducers/selector";
import FindPage from "../../../components/FindPage";
import Schedule from "../../Schedule";

const useStyles = makeStyles(theme => ({

}))

const TestSwitcher = ({}) => {

	const classes = useStyles()
	const group = useGroup()

	const displaySelect = () => {
		switch (group.groupId) {
			case 0: {
				return <FindPage/>

			}
			default: {
				return <Schedule/>

			}
		}
	}

	return (
		<React.Fragment>
			{ displaySelect() }
		</React.Fragment>
	)
}

TestSwitcher.propTypes = {}

export default TestSwitcher