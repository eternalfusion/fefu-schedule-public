import React from 'react'
import { Helmet } from 'react-helmet'
import { Switch, Route } from 'react-router-dom'
import HomePage from '../HomePage'
import NotFoundPage from '../NotFoundPage'
import { makeStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
// import PropTypes from 'prop-types'

//const useStyles = makeStyles(theme => ({}))

// Import web worker hooks
import { useWorker } from 'react-hooks-worker';




const App = ({}) => {
	//const createWorker = () => new Worker('../Schedule.worker.js', { type: 'module' })
	//const [c, e] = useWorker(createWorker, [])

	const blob = new Blob([
		`self.func = console.log('Starting Web Worker for PWA...');`,
		'self.onmessage = (e) => {',
		'  const result = self.func(e.data);',
		'  self.postMessage(result);',
		'};',
	], { type: 'text/javascript' });
	const url = URL.createObjectURL(blob);
	const createWorker = () => new Worker(url);
	//const classes = useStyles()
	createWorker()
	return (
		<React.Fragment>
			<CssBaseline/>

			<Helmet
				titleTemplate="%s - Расписание ДВФУ"
				defaultTitle="Расписание ДВФУ"
			>
				<meta name="description" content="Расписание ДВФУ"/>
			</Helmet>

			<Switch>
				<Route exact path="/" component={ HomePage }/>
				<Route path="/schedule" component={ HomePage }/>
				<Route path="" component={ NotFoundPage }/>
			</Switch>
		</React.Fragment>
	)
}

App.propTypes = {}

export default App