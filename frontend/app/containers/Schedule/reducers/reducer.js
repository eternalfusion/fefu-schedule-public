import produce from 'immer'

import {CHANGE_WEEK_ARRAY, CHANGE_WEEK_DAY, TOGGLE_PARITY} from './constants'
import {format, getDay, addWeeks, getWeek} from "date-fns";

import ScheduleWeek from "../ScheduleWeek"

const date = new Date()
let weekArray = [], weekDay = getDay(date), parity = false

weekArray = ScheduleWeek(date)

const todayFormat = Number(format(date, 'i'))

parity = !(getWeek(new Date(), {weekStartsOn: 1}) % 2)

let nextWeek = false

weekArray.some((item, index) => {
	if (todayFormat === index + 1) {
		nextWeek = false
		return true
	} else if (todayFormat > index + 1) {
		nextWeek = true
	}
})
if (nextWeek) {
	weekDay = 1
	parity = !parity
	weekArray = ScheduleWeek(addWeeks(date, 1))
}


export const initialState = {
	weekDay,
	weekArray,
	parity,
}




/* eslint-disable default-case, no-param-reassign */
const weekReducer = (state = initialState, action) =>
	produce(state, draft => {
		//console.log(action)
		switch (action.type) {
			case CHANGE_WEEK_DAY:
				draft.weekDay = action.weekDay
				break
			case CHANGE_WEEK_ARRAY:
				draft.weekArray = action.weekArray
				break
			case TOGGLE_PARITY:
				draft.parity = action.parity
				break
		}
	})

export default weekReducer
