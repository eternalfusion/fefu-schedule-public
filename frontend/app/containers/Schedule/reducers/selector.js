import { shallowEqual, useSelector } from 'react-redux'

export const useWeek = () => {
	return useSelector(state => state.week, shallowEqual)
}

export const useWeekDay = () => {
	return useSelector(state => state.week.weekDay, shallowEqual)
}

export const useWeekParity = () => {
	return useSelector(state => state.week.parity, shallowEqual)
}

export const useWeekArray = () => {
	return useSelector(state => state.week.weekArray, shallowEqual)
}