import { CHANGE_WEEK_DAY, CHANGE_WEEK_ARRAY, TOGGLE_PARITY } from './constants'

export const changeWeekDay = weekDay => {
	return {
		type: CHANGE_WEEK_DAY,
		weekDay,
	}
}

export const changeWeekArray = weekArray => {
	return {
		type: CHANGE_WEEK_ARRAY,
		weekArray,
	}
}


export const toggleParity = parity => {
	return {
		type: TOGGLE_PARITY,
		parity,
	}
}