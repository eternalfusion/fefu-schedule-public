import React, { useEffect, useState } from 'react'
// import PropTypes from 'prop-types'
import _ from 'lodash';
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import axios from "axios";
import {useGroup} from "../../components/ReactSelect/reducers/selector";
import DefaultHeader from "../../components/Header";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import WeeksPanel from "./components/WeeksPanel";
import {useWeekArray, useWeekDay} from "./reducers/selector";
import {format, addDays, getISODay} from "date-fns";
import SwipeableViews from "react-swipeable-views";
import BottomNavigation from "@material-ui/core/BottomNavigation/BottomNavigation";
import useTheme from "@material-ui/core/styles/useTheme";
import LessonBlock from "./components/LessonBlock";
import ChillBlock from './components/ChillBlock'

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		height: '100%',
		width: '100%'
	},
	toolbar: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		paddingTop: theme.spacing(10),
		'&': {
			marginBottom: theme.spacing(5)
		}
		//padding: theme.spacing(3),
	},
}))

const UrlFefuAPIWrapper = (data, array) => {
	//return `/api/fefu?group=` + encodeURIComponent(data.groupId) + `&start=` + encodeURIComponent(`2019-09-16T00:00:01`) + `&end=` + encodeURIComponent(`2019-09-22T00:00:05`)
	return `/api/fefu?group=` + encodeURIComponent(data.groupId) + `&start=` + encodeURIComponent(formatDate(array[0])) + `&end=` + encodeURIComponent(formatDate(addDays(array[array.length - 1], 1)))
}

const formatDate = (date) => {
	return format(new Date(date), 'yyyy-MM-dd') + 'T00:00:00'
}

const weekDaysRu = {
	0: 'ПН',
	1: 'ВТ',
	2: 'СР',
	3: 'ЧТ',
	4: 'ПТ',
	5: 'СБ'
}

const sortEventByDate = (events) => {
	const tmpArray = []
	for (let i = 0; i <= 5; i++) {
		tmpArray[i] = []
	}
	events.forEach((item, index) => {
		const day = getISODay(new Date(item.start)) - 1
		if (!tmpArray.hasOwnProperty(day))
			tmpArray[day] = []
		// console.log(item)
		// if (item[index - 1] && (item[index - 1].order === item[index].order)) {
		// 	item = {
		// 		pps_load: [item[index - 1].pps_load, item[index].pps_load],
		// 		order: item[index],
		// 		disciplineId: [item[index - 1].disciplineId, item[index].disciplineId],
		// 		teacher: [item[index - 1].teacher, item[index].teacher],
		// 		id: [item[index - 1].id, item[index].id],
		// 		start: item[index].end,
		// 		end: item[index].end,
		// 		classroom: [item[index - 1].classroom, item[index].classroom],
		// 		subgroup: [item[index - 1].subgroup, item[index].subgroup],
		// 	}
		// 	tmpArray[day].pop()
		// }
		tmpArray[day].push(item)
	})
	// tmpArray.forEach((item, index) => {
	// 	item.reduce()
	// }
	const orderArray = []
	tmpArray.map((item, index) => {
		// console.log(item)
		// if (item[index - 1] && (item[index - 1].order === item[index].order)) {
		// 	item = {
		// 		pps_load: [item[index - 1].pps_load, item[index].pps_load],
		// 		order: item[index],
		// 		disciplineId: [item[index - 1].disciplineId, item[index].disciplineId],
		// 		teacher: [item[index - 1].teacher, item[index].teacher],
		// 		id: [item[index - 1].id, item[index].id],
		// 		start: item[index].end,
		// 		end: item[index].end,
		// 		classroom: [item[index - 1].classroom, item[index].classroom],
		// 		subgroup: [item[index - 1].subgroup, item[index].subgroup],
		// 	}
		// 	const day = getISODay(new Date(item.start)) - 1
		// 	tmpArray[day].pop()
		// }
		// item.sort(function(a, b) {
		// 	return a.order - b.order
		// })

		// let t = _.map(_.groupBy(item, "order"), function(vals, order) {
		// 	return _.reduce(vals, function(m, o) {
		// 		for (let p in o)
		// 			if (p !== "order")
		// 				m[p] = (m[p])+ [p];
		// 		return m;
		// 	}, {order: order});
		// });
		// console.log(t)
		const tmpOrder = []
		item.forEach((lesson, i) => {
			// if (!tmpOrder[lesson.order]) {
			// 	tmpOrder[lesson.order] = []
			// 	tmpOrder[lesson.order].push(lesson)
			// }
			// else tmpOrder[lesson.order].push(lesson)
			if (!tmpOrder[lesson.order]) {
				lesson.pps_load = getFormatedPps(lesson.pps_load)
				tmpOrder[lesson.order] = lesson
			} else {
				if (tmpOrder[lesson.order].title.length > 32) {
					tmpOrder[lesson.order].title = tmpOrder[lesson.order].title.substring(0, 32) + "... | " + lesson.title.substring(0, 32) + "..."
					tmpOrder[lesson.order].titleFull = tmpOrder[lesson.order].titleFull + " | " + lesson.titleFull
				} else {
					tmpOrder[lesson.order].title = tmpOrder[lesson.order].title + " | " + lesson.title
				}
				tmpOrder[lesson.order].subgroup = tmpOrder[lesson.order].subgroup + ", " + lesson.subgroup
				if (getFormatedPps(tmpOrder[lesson.order].pps_load) !== getFormatedPps(lesson.pps_load)) {
					tmpOrder[lesson.order].pps_load = getFormatedPps(tmpOrder[lesson.order].pps_load) + " | " + getFormatedPps(lesson.pps_load)
				}
				if(tmpOrder[lesson.order].teacher.length > 0) tmpOrder[lesson.order].teacher = tmpOrder[lesson.order].teacher + ", " + lesson.teacher
				if(tmpOrder[lesson.order].classroom.length > 0) tmpOrder[lesson.order].classroom = tmpOrder[lesson.order].classroom + ", " + lesson.classroom
			}
		})
		orderArray[index] = tmpOrder
	})


	//console.log("or")
	//console.log(orderArray)
	return orderArray
}

const getFormatedPps = (pps) => {
	switch (pps) {
		case 'Лекционные занятия': {
			return 'Лекция'
		}
		case 'Практические занятия': {
			return 'Практика'
		}
		case 'Лабораторные работы': {
			return 'Лабораторная'
		}
		case 'Самостоятельная работа': {
			return 'С/р'
		}
		default: {
			return pps
		}
	}
}



const Schedule = ({}) => {
	const classes = useStyles()



	const group = useGroup()
	const weekArray = useWeekArray()
	//const [events, setEvents] = useState([]) //By day
	const [eventsFormated, setEventsFormated] = useState([]) //By day
	//const lessonsFormated = []

	const fetchScheduleForGroup = () => {
		const url = UrlFefuAPIWrapper(group, weekArray)
		let events = [], eventsFormatedTmp = []

		axios({url, method: 'get',})
			.then((response) => {
				//setEvents(sortEventByDate(response.data.events))
				events = sortEventByDate(response.data.events)
				console.log(events)
				// events.forEach((evt, index) => {
				// 		//console.log(new Date(lesson.start))
				// 		//console.log(getISODay(new Date(lesson.start)))
				// 		eventsFormatedTmp.push(
				// 			(evt.length > 0) ? (
				// 				<LessonBlock key={index} evt={evt}/>
				// 			) : (
				// 				<div key={index}>
				// 					Пар нет. Ура!!!
				// 				</div>
				// 			)
				// 		)
				// 	}
				//
				// )
				events.forEach((evt, index) => {
						//console.log(new Date(lesson.start))
						//console.log(getISODay(new Date(lesson.start)))

						eventsFormatedTmp.push(
							(evt.length > 0) ? (
								<LessonBlock key={index} evt={evt}/>
							) : (
								<ChillBlock key={index}>Пар в этот день нет.</ChillBlock>
							)
						)
					}

				)
				setEventsFormated(eventsFormatedTmp)
			})
			.catch((err) => {
				console.log(err)
			})
	}

	useEffect(() => {
		fetchScheduleForGroup()
	}, [group, weekArray])



	const weekDay = useWeekDay()
	const [value, setValue] = React.useState(weekDay - 1)

	const handleChange = (event, newValue) => {
		setValue(newValue)
	}

	const handleChangeIndex = (v) => {
		setValue(v)
	}

	return (
		<div className={ classes.root }>

			<DefaultHeader onUpdate={fetchScheduleForGroup}/>
			{
				(eventsFormated.length > 0) ? (
					<SwipeableViews
						axis='x'
						index={value}
						onChangeIndex={handleChangeIndex}
						className={classes.content}
						disableLazyLoading
					>
						{ eventsFormated }
					</SwipeableViews>
				) : (
					<ChillBlock>Расписание ещё не загружено...</ChillBlock>
				)
			}


			<WeeksPanel weekDaysRu={weekDaysRu} value={value} handleChange={handleChange}/>
		</div>
	)
}

Schedule.propTypes = {}

export default Schedule