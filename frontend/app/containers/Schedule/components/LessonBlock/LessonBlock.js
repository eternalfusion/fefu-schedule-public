import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {makeStyles} from '@material-ui/core/styles'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {format, differenceInHours, differenceInMinutes} from "date-fns";
import clsx from 'clsx'
import { Icon } from '@material-ui/core'
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ChillBlock from '../ChillBlock'
import decOfNum from '../../decOfNum'

const useStyles = makeStyles(theme => ({

	root: {
		marginBottom: '40px'
	},
	item: {
		//margin: `0 5px`,
		padding: 0,
		marginBottom: '5px'
	},
	title: {
		// width: 'calc(100vw - 50px)',
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},
	cardContent: {
		padding: '0 15px 15px 15px',
	// 	'&': {
	// 		padding: theme.spacing(1),
	// 	},
		'&:last-child': {
			paddingBottom: '15px',
		},
	},
	card: {
		boxShadow: 'none',
		borderRadius: 0,
		borderBottom: "solid 1px #eee"
	},
	cardTop: {
		margin: '15px 0',
		display: 'flex',
		justifyContent: 'space-between'
	},
	cardTopFlexBox: {
		display: 'flex',
		flexDirection: 'row'
	},
	cardNumber: {
		[theme.breakpoints.up("md")]: {
			width: '65px',
		},
		[theme.breakpoints.down("md")]: {
			width: '50px',
		},

		background: '#eee',
		textAlign: 'right',
		borderTopRightRadius: '20px',
		borderBottomRightRadius: '20px',
		marginRight: theme.spacing(2),
		'&': {
			paddingRight: '20px'
		}
	},
	cardPps: {
		color: 'rgba(0, 0, 0, 0.54)',
		[theme.breakpoints.up("md")]: {
			fontSize: '1.2rem'
		},
		[theme.breakpoints.down("md")]: {
			fontSize: '1.1rem'
		},

		[theme.breakpoints.down("sm")]: {
			fontSize: '0.85rem'
		},
		//lineHeight: '0'
	},
	time: {
		marginRight: '15px'
	},
	classroom: {
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: theme.spacing(1.5)
	}


}))

const windowComponent = (index, o) => {

}

const LessonBlock = ({evt}) => {

	const classes = useStyles()

	//const [isLessonStarted, setIsLessonStarted] = useState(false)
	//let prevInd = 100
	let prevInd = 100
	let prevLessonEnd = ""


	return (
		<Grid container className={classes.root} spacing={0}>
			{

				evt.map((lesson, index) => {



						//prevInd = index
						//console.log(prevInd)
						let tmpIndDiff = index - prevInd
						prevInd = index
						return (

							<Grid item xs={ 12 } key={ index } className={ classes.item }>
								{
									// (tmpIndDiff > 1) ?
									// <ChillBlock>
									// 	Пара кончается в { format(new Date(lesson.end), 'HH:mm') }
									// </ChillBlock> : ""
										(()=> {
											if (tmpIndDiff > 1) {
												//prevLessonEnd = lesson.end
												let d = differenceInMinutes(new Date(lesson.start), new Date(prevLessonEnd))
												let h = Math.floor(d / 60)
												return <ChillBlock>
													Пара заканчивается в { format(new Date(prevLessonEnd), 'HH:mm') },
													будет окно в { h } { decOfNum(h, ['час', 'часа', 'часов']) } { d - h * 60 } { decOfNum(d - h * 60, ['минута', 'минуты', 'минут']) }
												</ChillBlock>
											}
											prevLessonEnd = lesson.end
										})()
								}
								<Card className={ classes.card }>
									<div className={ classes.cardTop }>
										<div className={ classes.cardTopFlexBox }>
											<div className={ classes.cardNumber }>{ index }</div>
											<div className={ classes.cardPps }>
												{ lesson.pps_load } { lesson.subgroup && lesson.subgroup !== "" ? " (" + lesson.subgroup + ")" : "" }
											</div>
										</div>
										<div className={ clsx(classes.cardPps, classes.time) }>
											{
												format(new Date(lesson.start), 'HH:m') + "-" + format(new Date(lesson.end), 'HH:mm')
											}
										</div>
									</div>
									<CardContent className={ classes.cardContent }>
										<Typography className={ classes.title } variant="h6" component="h2"
										            gutterBottom>

											{ lesson.title }
										</Typography>
										<Typography className={ classes.pos } variant="h5" component="h2"
										            color="textSecondary">
											{ lesson.teacher ? lesson.teacher : 'Преподаватель не указан' }
										</Typography>

										<Typography className={ clsx(classes.pos, classes.classroom) } variant="h5" component="h2"
										            color="textSecondary">
											<LocationOnIcon fontSize="small"/>{ lesson.classroom && lesson.classroom !== "" ? lesson.classroom : "Аудитория не указана" }
										</Typography>

									</CardContent>
								</Card>
							</Grid>
						)

				})
			}
		</Grid>
	)
}

LessonBlock.propTypes = {
	evt: PropTypes.array.isRequired,
}

export default LessonBlock

// const LessonBlock = ({evt}) => {
//
// 	const classes = useStyles()
//
// 	return (
// 		<Grid container className={classes.root} spacing={2}>
// 			{
// 				evt.map((lesson, index) => {
// 					return (
// 						<Grid item xs={12} key={index} className={classes.item}>
// 							<Card className={classes.card}>
// 								<CardContent>
// 									<Typography className={classes.title} variant="h5" component="h2" gutterBottom >
// 										{lesson.title}{lesson.subgroup && lesson.subgroup !== "" ? " ("+lesson.subgroup + ")" : ""}{lesson.classroom && lesson.classroom !== "" ? " | "+lesson.classroom : ""}
// 									</Typography>
// 									<Typography className={classes.pos}>
// 										{lesson.teacher?lesson.teacher:'Преподователь не указан'}
// 									</Typography>
// 									<Typography variant="body2" component="p" color="textSecondary">
// 										{lesson.pps_load}
// 									</Typography>
// 									{
// 										format(new Date(lesson.start), 'HH:m') + "-" + format(new Date(lesson.end), 'HH:mm')
// 									}
// 								</CardContent>
// 							</Card>
// 						</Grid>
// 					)
// 				})
// 			}
// 		</Grid>
// 	)
// }