import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import {makeStyles} from '@material-ui/core/styles'
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import {format} from "date-fns";
import {useWeek, useWeekArray, useWeekDay} from "../../reducers/selector";
import useTheme from "@material-ui/core/styles/useTheme";
import SwipeableViews from 'react-swipeable-views'

function TabPanel(props) {
	const {children, value, index, ...other} = props;

	return (
		<Typography
			component="div"
			role="tabpanel"
			hidden={value !== index}
			id={`scrollable-auto-tabpanel-${index}`}
			aria-labelledby={`scrollable-auto-tab-${index}`}
			{...other}
		>
			<Box p={3}>{children}</Box>
		</Typography>
	);
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
};




const useStyles = makeStyles(theme => ({
	// root: {
	// 	flexGrow: 1,
	// 	width: '100%',
	// 	backgroundColor: theme.palette.background.paper,
	// },
	WeeksPanel: {
		bottom: 0,
		top: 'auto',
	},
	bottomNavigation: {
		minWidth: 'unset',
	},
}));




const WeeksPanel = ({weekDaysRu, value, handleChange}) => {
	const classes = useStyles()
	const week = []

	const weekArray = useWeekArray()

	weekArray.forEach((item, index) => {
		//console.log(item)
		week.push(<BottomNavigationAction className={classes.bottomNavigation} value={index} key={index} label={weekDaysRu[index]} icon={format(new Date(item), 'dd.MM')} />)
	})


	return (
		<AppBar position="fixed" className={classes.WeeksPanel}>
			<BottomNavigation
				value={value}
				onChange={handleChange}
				showLabels
				variant="fullWidth"
			>
				{/*<BottomNavigationAction label="ПН" />*/}
				{/*<BottomNavigationAction label="ВТ" />*/}
				{/*<BottomNavigationAction label="СР" />*/}
				{/*<BottomNavigationAction label="ЧТ" />*/}
				{/*<BottomNavigationAction label="ПТ" />*/}
				{/*<BottomNavigationAction label="СБ" />*/}
					{week}
			</BottomNavigation>
		</AppBar>
	)
}

WeeksPanel.propTypes = {
	weekDaysRu: PropTypes.object.isRequired,
	value: PropTypes.number.isRequired,
	handleChange: PropTypes.func.isRequired,
}

export default WeeksPanel