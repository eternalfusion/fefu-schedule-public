import React from 'react'
import PropTypes from 'prop-types'
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({

	root: {
		minHeight: '110px',
		padding: '15px',
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center'
	},
	text: {
		fontWeight: 500,
		color: 'rgba(0,0,0,.54)',
		marginLeft: '15px'
	}


}))


const ChillBlock = ({ children }) => {

	const classes = useStyles()

	return (
		<div className={classes.root}>
			<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><path fill="#FFC107" d="M11.143 15L13 28 35 28 36.857 15z"></path><path fill="#795548" d="M39,15v-2.5c0-0.828-0.672-1.5-1.5-1.5h-27C9.672,11,9,11.672,9,12.5V15H39z"></path><path fill="#77574F" d="M13.143 29L15 42 33 42 34.857 29z"></path><path fill="#77574F" d="M24 23A5 5 0 1 0 24 33A5 5 0 1 0 24 23Z"></path><path fill="#4E342E" d="M20 28c0-.732.211-1.409.555-2h-7.84l.714 5h7.951C20.541 30.267 20 29.201 20 28zM27.445 26C27.789 26.591 28 27.268 28 28c0 1.201-.541 2.267-1.38 3h7.951l.714-5H27.445zM36 6L12 6 11 11 37 11z"></path><path fill="#5D4037" d="M24,21c3.866,0,7,3.134,7,7s-3.134,7-7,7s-7-3.134-7-7S20.134,21,24,21 M24,20c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S28.411,20,24,20L24,20z"></path><path fill="#FFF3E0" d="M24,21c-3.866,0-7,3.134-7,7s3.134,7,7,7s7-3.134,7-7S27.866,21,24,21z M24,32c-2.209,0-4-1.791-4-4s1.791-4,4-4s4,1.791,4,4S26.209,32,24,32z"></path></svg>
			<div className={classes.text}>{ children }</div>
		</div>
	)
}

ChillBlock.propTypes = {
	//evt: PropTypes.array.isRequired,
	start: PropTypes.string,
	end: PropTypes.string,
}

export default ChillBlock

// const LessonBlock = ({evt}) => {
//
// 	const classes = useStyles()
//
// 	return (
// 		<Grid container className={classes.root} spacing={2}>
// 			{
// 				evt.map((lesson, index) => {
// 					return (
// 						<Grid item xs={12} key={index} className={classes.item}>
// 							<Card className={classes.card}>
// 								<CardContent>
// 									<Typography className={classes.title} variant="h5" component="h2" gutterBottom >
// 										{lesson.title}{lesson.subgroup && lesson.subgroup !== "" ? " ("+lesson.subgroup + ")" : ""}{lesson.classroom && lesson.classroom !== "" ? " | "+lesson.classroom : ""}
// 									</Typography>
// 									<Typography className={classes.pos}>
// 										{lesson.teacher?lesson.teacher:'Преподователь не указан'}
// 									</Typography>
// 									<Typography variant="body2" component="p" color="textSecondary">
// 										{lesson.pps_load}
// 									</Typography>
// 									{
// 										format(new Date(lesson.start), 'HH:m') + "-" + format(new Date(lesson.end), 'HH:mm')
// 									}
// 								</CardContent>
// 							</Card>
// 						</Grid>
// 					)
// 				})
// 			}
// 		</Grid>
// 	)
// }