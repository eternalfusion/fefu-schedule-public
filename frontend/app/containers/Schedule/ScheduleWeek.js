import {eachDayOfInterval, endOfWeek, startOfWeek} from "date-fns";


const ScheduleWeek = (date) => {
	const start = startOfWeek(date, {weekStartsOn: 1})
	const end = endOfWeek(date, {weekStartsOn: 1})

	const w = eachDayOfInterval({start: new Date(start), end: new Date(end)})
	w.pop()
	return w
}

export default ScheduleWeek
