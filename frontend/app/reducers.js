/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import themeReducer from 'components/ChangeableThemeProvider/reducers/reducer'
import history from './utils/history'
import languageProviderReducer from 'components/LanguageProvider/reducers/reducer'
import groupReducer from "./components/ReactSelect/reducers/reducer";
import weekReducer from "./containers/Schedule/reducers/reducer";
import parityProviderReducer from "./components/Header/reducers/reducer";

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
	const rootReducer = combineReducers({
		theme: themeReducer,
		language: languageProviderReducer,
		router: connectRouter(history),
		group: groupReducer,
		week: weekReducer,
		//parity: parityProviderReducer,
		...injectedReducers,
	})

	return rootReducer
}
