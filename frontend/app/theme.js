import { createMuiTheme } from '@material-ui/core/styles'
import { getContrastText } from '@material-ui/core/'
import createBreakpoints from '@material-ui/core/styles/createBreakpoints'

const breakpoints = createBreakpoints({})

export default type => createMuiTheme({
	
	/*typography: {
		fontFamily: [
			'Lexend Deca',
			'Roboto',
			'Helvetica',
			'Arial',
			'sans-serif',
		].join(','),
		letterSpacing: '0.02857em',
		button: {

			letterSpacing: '0.02857em',
		}
	},*/
	typography: {
		h5: {
			fontSize: '1.2rem',
			[breakpoints.up("md")]: {
				fontSize: '1.2rem'
			},
			[breakpoints.down("md")]: {
				fontSize: '1rem'
			},

			[breakpoints.down("sm")]: {
				fontSize: '0.8rem'
			}
		},
		h6: {
			fontSize: '1.2rem',
			[breakpoints.up("md")]: {
				fontSize: '1.3rem'
			},
			[breakpoints.down("md")]: {
				fontSize: '1.1rem'
			},

			[breakpoints.down("sm")]: {
				fontSize: '0.85rem'
			},
			marginTop: 0,
			marginBottom: 0
		},
		body2: {
			fontSize: '1.2rem',
			[breakpoints.up("md")]: {
				fontSize: '1.2rem'
			},
			[breakpoints.down("md")]: {
				fontSize: '1.rem'
			},

			[breakpoints.down("sm")]: {
				fontSize: '0.8rem'
			}
		},
	},
	palette: {
		types: {
		 	dark: {
		 		background: {
		 			default: '#121212'
		 		}
		 	},
		 	light: {
		 		background: {
		 			default: '#ffffff'
		 		},

		 	}
		},
		/*text: {
			primary: "#ffffff",
		},*/
		primary: {
			light: '#0096d5',
			main: '#0096d5',
			dark: '#0096d5',
			contrastText: '#ffffff',
		},
		type: type,
	},
})