import React, {useRef} from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import ReactSelect from "../ReactSelect";

import fefu from "../../fefu.json"

import Logotype from '../../images/logo/logo2.svg'

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		textAlign: 'center',
		background: '#0096d5',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',

	},
	find: {

	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	center: {
		minWidth:theme.spacing(50),
		[theme.breakpoints.down('xs')]: {
			minWidth: 100,
			width: '100%',
			padding: theme.spacing(5)
		},


	},
	logotype: {
		marginBottom: theme.spacing(10)
	},
}))


const { groups } = fefu


const FindPage = () => {
	const classes = useStyles();
/*
	const [values, setValues] = React.useState({
		age: '',
		name: 'hai',
	});

	const inputLabel = useRef(null);
	const [labelWidth, setLabelWidth] = React.useState(0);

	React.useEffect(() => {
		setLabelWidth(inputLabel.current.offsetWidth);
	}, []);

	function handleChange(event) {
		setValues(oldValues => ({
			...oldValues,
			[event.target.name]: event.target.value,
		}));
	}
*/

	return (
		<div className={classes.root}>
			<div className={classes.center}>
				<Logotype className={classes.logotype}/>
				<ReactSelect placeholder={"Начните вводить название..."} label={"Группа"} suggestions={groups}></ReactSelect>
			</div>

		</div>
	)
}

FindPage.propTypes = {
	children: PropTypes.any,
}

export default FindPage