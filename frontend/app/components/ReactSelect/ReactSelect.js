import React from 'react';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import {useGroup} from "./reducers/selector";
import {useDispatch} from "react-redux";
import {changeGroup} from "./reducers/actions";



function renderInputComponent(inputProps) {
	const { classes, inputRef = () => {}, ref, ...other } = inputProps;

	return (
		<TextField
			fullWidth
			InputProps={{
				inputRef: node => {
					ref(node);
					inputRef(node);
				},
				classes: {
					input: classes.input,
				},
			}}
			{...other}
		/>
	);
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
	const matches = match(suggestion.label, query);
	const parts = parse(suggestion.label, matches);

	return (
		<MenuItem selected={isHighlighted} component="div">
			<div>
				{parts.map(part => (
					<span key={part.text} style={{ fontWeight: part.highlight ? 500 : 400 }}>
            {part.text}
          </span>
				))}
			</div>
		</MenuItem>
	);
}

function getSuggestions(suggestions, value) {
	const inputValue = deburr(value.trim()).toLowerCase();
	const inputLength = inputValue.length;
	let count = 0;

	return inputLength === 0
		? []
		: suggestions.filter(suggestion => {
			const keep =
				//count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;
			      count < 5 && inputValue && match(suggestion.label, inputValue).length
			if (keep) {
				count += 1;
			}

			return keep;
		});
}

function getSuggestionValue(suggestion) {
	return suggestion.label;
}

const useStyles = makeStyles(theme => ({
	root: {
		//height: 250,
		flexGrow: 1,
	},
	container: {
		position: 'relative',
	},
	suggestionsContainerOpen: {
		position: 'absolute',
		zIndex: 1,
		marginTop: theme.spacing(1),
		left: 0,
		right: 0,
	},
	suggestion: {
		display: 'block',
	},
	suggestionsList: {
		margin: 0,
		padding: 0,
		listStyleType: 'none',
	},
	input: {
		color: '#fff',
		fontWeight: '700',
		'& .MuiFormLabel-root': {
			color: '#fff',
			fontWeight: '700',
		},
		'& .MuiFormControl-root ': {
			color: '#fff',
			fontWeight: '700',
		},
		'& label.Mui-focused': {
			color: '#fff',
			fontWeight: '700',
		},
		'& .MuiInput-underline:hover:not(.Mui-disabled):before': {
			borderBottomColor: '#fff',
		},
		'& .MuiInput-underline:before': {
			borderBottomColor: '#fff',
		},
		'& .MuiInput-underline:after': {
			borderBottomColor: '#fff',
		},
		'& .MuiOutlinedInput-root': {
			'& fieldset': {
				borderColor: 'red',
			},
			'&:hover fieldset': {
				borderColor: 'yellow',
			},
			'&.Mui-focused fieldset': {
				borderColor: '#fff',
			},
		}
	}
}));

const ReactSelect = ({label, placeholder, suggestions}) =>  {
	const classes = useStyles();

	const [state, setState] = React.useState({
		single: '',
	});

	const [stateSuggestions, setSuggestions] = React.useState([]);

	const handleSuggestionsFetchRequested = ({ value }) => {
		setSuggestions(getSuggestions(suggestions, value));
	};

	const handleSuggestionsClearRequested = () => {
		setSuggestions([]);
	};

	const handleChange = name => (event, { newValue }) => {
		setState({
			...state,
			[name]: newValue,
		});
	};

	const group = useGroup()
	const dispatch = useDispatch()

	const onSuggestionSelected = (event, {suggestion}) => {

		dispatch(changeGroup({
			groupId: suggestion.groupId,
			groupName: suggestion.label,
		}))




	}

	const autosuggestProps = {
		renderInputComponent,
		suggestions: stateSuggestions,
		onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
		onSuggestionsClearRequested: handleSuggestionsClearRequested,
		getSuggestionValue,
		renderSuggestion,
		onSuggestionSelected,
	};

	return (
		<div className={classes.root}>
			<Autosuggest
				{...autosuggestProps}
				inputProps={{
					classes,
					label: label,
					placeholder: placeholder,
					value: state.single,
					onChange: handleChange('single'),
				}}
				theme={{
					container: classes.container,
					suggestionsContainerOpen: classes.suggestionsContainerOpen,
					suggestionsList: classes.suggestionsList,
					suggestion: classes.suggestion,
					input: classes.input,
				}}
				renderSuggestionsContainer={options => (
					<Paper {...options.containerProps} square>
						{options.children}
					</Paper>
				)}
			/>
		</div>
	);
}

ReactSelect.propTypes = {
	classes: PropTypes.object,
	label: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,

}

export default ReactSelect