import produce from 'immer'

import { CHANGE_GROUP } from './constants'
import {useSelector} from "react-redux";
import useCookies from "react-cookie/es6/useCookies";
import Cookies from 'universal-cookie';

const cookies = new Cookies()

export const initialState = cookies.get('group') ? cookies.get('group') : {
	groupId: 0,
	groupName: '',
}

/* eslint-disable default-case, no-param-reassign */
const groupReducer = (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case CHANGE_GROUP:
				draft.groupId = action.groupId
				draft.groupName = action.groupName
				break
		}
	})

export default groupReducer
