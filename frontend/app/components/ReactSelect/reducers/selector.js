import { shallowEqual, useSelector } from 'react-redux'

export const useGroup = () => {
	return useSelector(state => state.group, shallowEqual)
}