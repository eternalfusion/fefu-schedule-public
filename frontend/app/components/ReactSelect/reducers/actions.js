import { CHANGE_GROUP } from './constants'

export const changeGroup = group => {
	return {
		type: CHANGE_GROUP,
		groupId: group.groupId,
		groupName: group.groupName,
	}
}