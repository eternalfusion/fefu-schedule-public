import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar } from '@material-ui/core'
import Hidden from '@material-ui/core/Hidden'
import Switch from "@material-ui/core/Switch";
import Logotype from '../../images/logo/logo2.svg'
import LogotypeSmall from '../../images/logo/logo1.svg'
import Typography from "@material-ui/core/Typography";
import {useGroup} from "../ReactSelect/reducers/selector";
import {useDispatch} from "react-redux";
import {useWeekArray, useWeekParity} from "../../containers/Schedule/reducers/selector";
import {changeWeekArray, toggleParity} from "../../containers/Schedule/reducers/actions";
import ScheduleWeek from "../../containers/Schedule/ScheduleWeek";
import {addWeeks} from "date-fns";
import UpdateIcon from '@material-ui/icons/Update';
import IconButton from '@material-ui/core/IconButton'
import SnackBar from '../SnackBar'

const useStyles = makeStyles(theme => ({
	toolbar: theme.mixins.toolbar,
	appBar: {
		transition: 'background-color 0.075s ease-in-out',
	},
	centeringAppbarItems: {
		display: 'flex',
		alignItems: 'center',
	},
	rightButtons: {
		marginLeft: 'auto',
		'& > *': {
			//marginLeft: theme.spacing(1.5)
		}
	},
	logotype: {
		height: 56,
		paddingTop: theme.spacing(1.25),
		paddingBottom: theme.spacing(1.25),
		[`${ theme.breakpoints.up('xs') } and (orientation: landscape)`]: {
			height: 48,
		},
		[theme.breakpoints.up('sm')]: {
			height: 64,
		},
	},
	groupName: {
		marginLeft: theme.spacing(1.5),
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
		[theme.breakpoints.down('xs')]: {
			fontSize: 18,
		},
	},
	parity: {
		width: 72,
		padding: 0,
		margin: 0,
		textAlign: 'end',
	},
	updateIconButton: {
		color: '#ffffff'
	}
}))

const DefaultHeader = ({ onUpdate }) => {

	const classes = useStyles()

	const group = useGroup()
	const parity = useWeekParity()
	const dispatch = useDispatch()
	const weekArray = useWeekArray()

	const handleParityChange = () => {
		const date = new Date(weekArray[0])
		dispatch(changeWeekArray(ScheduleWeek(addWeeks(date, parity?-1:1))))

		dispatch(toggleParity(!parity))

	}



	return (
		<AppBar
			position="fixed"
		>
			<SnackBar message="Test"></SnackBar>
			<Toolbar>
				<div className={ classes.centeringAppbarItems }>
					<Hidden smDown>
						<Logotype className={ classes.logotype } />
					</Hidden>
					<Hidden mdUp>
						<LogotypeSmall className={ classes.logotype } />
					</Hidden>
					<Typography variant="h5" className={ classes.groupName }>{group.groupName}</Typography>
				</div>


				<div className={ clsx(classes.rightButtons, classes.centeringAppbarItems) }>
					<IconButton aria-label="update" className={classes.updateIconButton} onClick={onUpdate}>
						<UpdateIcon/>
					</IconButton>
					<Switch value={ parity } checked={ parity } onChange={ handleParityChange } />
				</div>

			</Toolbar>
		</AppBar>
	)
}

DefaultHeader.propTypes = {
	onUpdate: PropTypes.func
}

export default DefaultHeader