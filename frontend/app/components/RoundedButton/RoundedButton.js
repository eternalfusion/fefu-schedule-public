import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles(theme => ({
	root: {
		background: 'transparent',
		borderRadius: 25,
	},
}))

const RoundedButton = ({ children }) => {
	const classes = useStyles()

	return (
		<Button
			classes={ { root: classes.root } }
			variant="outlined"

		>
			{ children }
		</Button>
	)
}

RoundedButton.propTypes = {
	children: PropTypes.any,
}

export default RoundedButton