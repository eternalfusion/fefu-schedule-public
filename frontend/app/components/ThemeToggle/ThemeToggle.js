import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { useDispatch } from 'react-redux'
import { useThemeType, changeTheme, THEME_TYPES } from '../ChangeableThemeProvider'

const ThemeToggle = ({ className }) => {

	const dispatch = useDispatch()

	const themeType = useThemeType()

	const handler = (event) => {
		if(themeType === THEME_TYPES.DARK) {
			dispatch(changeTheme(THEME_TYPES.LIGHT))
		} else {
			dispatch(changeTheme(THEME_TYPES.DARK))
		}
	}

	return (
		<div className={ className }>
			<Button
				onClick={ handler }
			>
				Toggle theme!
			</Button>
		</div>
	)
}

ThemeToggle.propTypes = {
	className: PropTypes.string,
}

export default ThemeToggle