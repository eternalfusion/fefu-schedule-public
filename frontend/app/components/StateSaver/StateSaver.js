import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import {useSelector} from "react-redux";
import {isEqual} from 'lodash'
import useCookies from "react-cookie/es6/useCookies";
import Cookies from "universal-cookie";

const cookies = new Cookies()


const StateSaver = ({children, selector}) => {
	const selectedState = useSelector(selector, isEqual)

	useEffect(()=>{
		cookies.set('group', JSON.stringify(selectedState), { path: '/' })
	}, [selectedState])

	return children
}

StateSaver.propTypes = {
	children: PropTypes.any,
	selector: PropTypes.func.isRequired
}

export default StateSaver