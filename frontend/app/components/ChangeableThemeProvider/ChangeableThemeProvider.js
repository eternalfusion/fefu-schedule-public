import React from 'react'
import PropTypes from 'prop-types'
import theme from '../../theme'
import { ThemeProvider } from '@material-ui/styles'
import DependentWrapper from './DependentWrapper'
import { useThemeType } from './reducers/selector'

const ChangeableThemeProvider = ({ children }) => {

	const themeType = useThemeType()

	return (
		<ThemeProvider
			theme={ theme(themeType) }
		>
			<DependentWrapper>
				{ React.Children.only(children) }
			</DependentWrapper>
		</ThemeProvider>
	)
}

ChangeableThemeProvider.propTypes = {
	children: PropTypes.element.isRequired,
}

export default ChangeableThemeProvider