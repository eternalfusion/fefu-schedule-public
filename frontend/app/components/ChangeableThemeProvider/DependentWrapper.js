import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

const useGlobalStyles = makeStyles(theme => ({
	'@global': {
		html: {
			width: '100%',
			height: '100%',
			padding: 0,
			margin: 0,
		},
		body: {
			// background: (theme.palette.type === 'light')
			// 	? theme.palette.types.light.background.default
			// 	: theme.palette.types.dark.background.default,
			background: theme.palette.background.default,
			color: theme.palette.text.primary,
			width: '100%',
			height: '100%',
			padding: 0,
			margin: 0,
		},
		'#app': {
			width: '100%',
			height: '100%',
			display: 'flex',
		},
	},
}))

const DependentWrapper = ({ children }) => {

	// Apply global styles
	useGlobalStyles()

	return React.Children.only(children)
}

DependentWrapper.propTypes = {
	children: PropTypes.element.isRequired,
}

export default DependentWrapper