import { CHANGE_THEME } from './constants'

export const changeTheme = themeType => {
	return {
		type: CHANGE_THEME,
		themeType: themeType,
	}
}