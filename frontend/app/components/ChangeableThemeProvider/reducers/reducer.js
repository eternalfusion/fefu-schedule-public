import produce from 'immer'

import { CHANGE_THEME, THEME_TYPES } from './constants'

export const initialState = {
	type: THEME_TYPES.LIGHT,
}

/* eslint-disable default-case, no-param-reassign */
const themeProviderReducer = (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case CHANGE_THEME:
				draft.type = action.themeType
				break
		}
	})

export default themeProviderReducer
