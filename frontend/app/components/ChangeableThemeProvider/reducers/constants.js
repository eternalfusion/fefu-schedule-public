export const CHANGE_THEME = 'StateSaver/CHANGE_Theme'

export const THEME_TYPES = Object.freeze({
	LIGHT: 'light',
	DARK: 'dark',
})