import React from 'react'
import PropTypes from 'prop-types'
import { IntlProvider } from 'react-intl'
import { useLocale } from './reducers/selector'

const LanguageProvider = ({ messages, children }) => {

	const locale = useLocale()

	return (
		<IntlProvider
			key={ locale }
			locale={ locale }
			messages={ messages[locale] }
		>
			{ React.Children.only(children) }
		</IntlProvider>
	)
}

LanguageProvider.propTypes = {
	messages: PropTypes.object,
	children: PropTypes.element.isRequired,
}

export default LanguageProvider