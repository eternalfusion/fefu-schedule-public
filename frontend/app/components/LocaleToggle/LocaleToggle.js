import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { useDispatch } from 'react-redux'
import { useLocale, changeLocale } from '../LanguageProvider'
import IconButton from '@material-ui/core/IconButton'
import Translate from '@material-ui/icons/Translate'

const LocaleToggle = ({ className }) => {

	const dispatch = useDispatch()

	const locale = useLocale()

	const handler = (event) => {
		if(locale === 'ru') {
			dispatch(changeLocale('en'))
		} else {
			dispatch(changeLocale('ru'))
		}
	}

	return (
		<IconButton
			className={ className }
			onClick={ handler }
		>
			<Translate
				fontSize="small"
			/>
		</IconButton>
	)
}

LocaleToggle.propTypes = {
	className: PropTypes.string,
}

export default LocaleToggle