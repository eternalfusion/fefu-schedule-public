import { exposeWorker } from 'react-hooks-worker'

exposeWorker(() => {
	console.log('Start Web Worker for creating PWA...')
})